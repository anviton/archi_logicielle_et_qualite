import java.util.Iterator;

import alea.MTRandom;

public class Agent {
    private Statut statut;
    private int statutTempsEcoule;
    private final int dE;
    private final int dI;
    private final int dR;

    public Agent(Statut statut, MTRandom mtRandom){
        dE = (int)(-3 * java.lang.Math.log(1 - mtRandom.nextDouble()));        
        dI = (int)(-7 * java.lang.Math.log(1 - mtRandom.nextDouble()));        
        dR = (int)(-365 * java.lang.Math.log(1 - mtRandom.nextDouble()));
        this.statut = statut;
        statutTempsEcoule = 0;
    }

    public Statut getStatut()
    {
        return statut;
    }

    public void deplacer(int posXAgent, int posYAgent, MTRandom mtRandom, Grille grille, 
    Iterator<Agent> iterator)
    {
        int nouvX, nouvY;
        nouvX = mtRandom.nextInt(0, grille.getDimension());        
        nouvY = mtRandom.nextInt(0, grille.getDimension());
        if(nouvX != posXAgent && nouvY != posYAgent){
            iterator.remove();
            grille.getCellules()[nouvX][nouvY].ajouterAgent(this);
        }
    }

    // à chaque pas de temps : S vérifient si leurs voisins sont des I
    public void verifierVoisinage(Grille grille, int posXAgent, int posYAgent, MTRandom mtRandom)
    {
        int nbVoisinI = 0;
        double proba, tirage;
        int posX, posY, dimension = grille.getDimension();
        for(int i = -1; i <= 1; i++){    
            for(int j = -1; j <= 1; j++){
                posX = (posXAgent + i + dimension)  % dimension;
                posY = (posYAgent + j + dimension) % dimension;
                for (Agent vAgent : grille.getCellules()[posX][posY].getAgents()) {
                    if(vAgent.getStatut() == Statut.I){
                        nbVoisinI++;
                    }
                }
            }
        }
        proba = 1 - java.lang.Math.exp(-0.5 * nbVoisinI);
        tirage = mtRandom.nextDouble(0, 1);
        if(tirage < proba){
            statut = Statut.E;
            statutTempsEcoule = 0;
        }
    }

    public void verifierStatut(int nbStatut[]){
        switch (statut) {
            case E:
                nbStatut[0]++;
                if(statutTempsEcoule > dE){
                    statut = Statut.I;
                    statutTempsEcoule = 0;

                }
                break;
            case I:
                nbStatut[1]++;
                if(statutTempsEcoule > dI){
                    statut = Statut.R;
                    statutTempsEcoule = 0;

                }
                break;
            case R:
                nbStatut[2]++;
                if(statutTempsEcoule > dR){
                    statut = Statut.S;
                    statutTempsEcoule = 0;
                }
                break;
            default:
                break;
        }
    }

    public void incrementStatutTempsEcoule(){
        statutTempsEcoule++;
    }

}