import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class SauveurEtatGrilleCSV extends SauveurEtatGrille {

    private FileWriter fileWriter;
    private BufferedWriter writer;

    public SauveurEtatGrilleCSV(int numSimu){
        try {
            File fichier = new File("res/Simulation" + numSimu + ".CSV");

            // Vérifiez si le fichier n'existe pas
            if (!fichier.exists()) {
                fichier.createNewFile();
            }
            fileWriter = new FileWriter(fichier);
            writer = new BufferedWriter(fileWriter);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    @Override
    public void sauverEtat(int[] nbStatut) {
        try {
            writer.write(nbStatut[0] + ";" + nbStatut[1] + ";" + nbStatut[2] + ";" + nbStatut[3]);
            writer.newLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void fermerWriter(){
        try {
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
}
