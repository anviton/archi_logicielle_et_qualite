
public class Grille {
    
    private Cellule cellules[][];
    private int dimension;
    
    public Grille(int dimension){
        this.dimension = dimension;
        cellules = new Cellule[dimension][dimension];
        for(int i = 0; i < dimension; i++){
            for(int j = 0; j < dimension; j++){
                cellules[i][j] = new Cellule();
            }
        } 
    }
    
    public int getDimension(){
    	return dimension;
    }

    public Cellule[][] getCellules(){
        return cellules;
    }

}
