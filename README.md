# Archi_logicielle_et_qualite

## Auteurs
VITON Antoine / MECHOUD Louane

## Comment utiliser le projet ⚙️

(en se plaçant dans le dossier source)

### Compilation

javac -d bin -sourcepath src src/Main.java

### Execution

java -cp bin Main

## Projet

L’objectif est le développement d'un modèle multi-agent simple de propagation d'une maladie dans une population.

### Diagramme de classes 📝
Tout d'abord voici le diagramme de classes correspondant à l'énoncé ci-dessus :

![Image](documentation/diagramme.svg)

### Explication Classes

#### Agent
Les individus sont représentés à l'aide d'une classe Agent, caractérisées par deux états discrets : leur statut et le temps écoulé dans ce statut, ainsi que par trois paramètres de durée : dE, dR et dI.

#### Etats Agents
Le fichier Statut.java contient une énumération représentant tous les statuts qu'un agent peut avoir, que sont R, S, I et E, (R recovered, S suceptible, I infecté, E exposé).

#### Grille
Nous avons placé ces individus dans une classe Grille qui contient un attribut dimension, ainsi qu'un tableau de cellules.

#### Cellule
Les cellules sont représentées par une classe Cellule composée de coordonnées et d'une liste d'agents, nous pouvons alors  ajouter et supprimer des agents pour chaque cellule.

### Simulation
Réprésente le coeur de l'application, permet d'initialiser et de lancer une simulation.


### Déroulement d'une Simulation

#### Initialisation
On intialise la simulation : 
- la grille avec sa dimension (nombre de cellules = dimension * dimension)
- le nombre d'agents 20 dans le statut I (infecté) et 19980 dans le statut S (suceptible)
- les agents sont placès aléatoirement dans les cellules.

#### Cheminement Simulation

##### Déplacement
A chaque pas de temps, tous les individus se déplacent aléatoirement vers une autre cellule. La nouvelle position est choisie au hasard dans la grille (pas nécessairement les cellules voisines).


##### Changement d'état
A chaque pas de temps on fait évoluer le statut de chaque individu deux cas possibles :
- L'individu est suceptible alors on vient compter les individus infectés dans son voisinage et on détermine grâce à la formule donnée dans l'énoncé la probabilité qu'il devienne exposé, ensuite on tire un nombre aléatoire entre 0 et 1 pour déterminer s'il l'individu devient infecté ou non.
- L'individu n'est pas dans l'état suceptible on vérifie si le temps passé dans son statut est supérieur au temps que dure ce statut si c'est le cas on le change de statut


### Génération aléatoire
Afin de générer les postions des agents, leur déplacement aléatoire, le temps passé dans chaque statut pour chaque agent et pour déterminer si un agent suceptible doit passer dans le statut exposé, nous avons utilisé l'implémentation java du générateur Mersenne Twister de Matsumoto. Nous avons bien veillé à utliser une seule instance du genérateur dans tout le code.

### Sauvegarde des résultats

Nous avons mis en place le patron stratégie pour sauvegarder les résultats avec une implémentation concrète la sauvegarde au format CSV (il est possibles d'ajouter d'autres implémentations concrètes et ainsi de sauver les données en utilisant un autre format). Les résultats au format CSV sont enregistrés dans le dossier source/res/ et nous avons un fichier pour chaque simulation (soit 100 fichiers).

### Résultats 📊
Le code génère alors des fichiers contenant les résultats du nombre d'agents dans l'état S, E, I et R pour les 730 itérations.
A l'aide de Jupyter Notebook, nous avons pu lire ces résultats et afficher un graphique des moyennes de 100 simulations :
Le Notebook se trouve à la racine du projet (fichier nommé Architecture_Projet.ipynb).

![Image](documentation/moyenne_simu.png)

Ce résultat est conforme à nos attentes.

Cependant pour la simulation numéro 83 nous avons eu un résultat différent :

![Image](documentation/simu_numero83.png)

On peut imaginer que l'épidémie n'a pas vraiment "pris" dans cette situation, peut-être dû à un placement "particulier" des individus infectés au début de la simulation.


