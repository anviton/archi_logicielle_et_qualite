public abstract class SauveurEtatGrille {
    
    public abstract void sauverEtat(int nbStatut[]);

    public abstract void fermerWriter();
}
