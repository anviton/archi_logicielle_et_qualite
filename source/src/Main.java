import alea.*;

public class Main {

    public static void main(String[] args) {
        MTRandom gen = new MTRandom();
        gen.setSeed(new int[] {0x123, 0x234, 0x345, 0x456});
        Simulation simulation;
        for(int i  = 0; i < 100; i++){
            simulation = new Simulation(i, 730, gen);
            simulation.lancerSimulation();
        }
    }
}