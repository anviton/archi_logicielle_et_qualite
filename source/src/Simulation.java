import java.util.Iterator;

import alea.*;

/**
 * Classe Simulation permet de simuler le modèle multi-agents
 * @author anviton lomechoud
 */
public class Simulation {

    private Grille grille;
    private int tempsEcoule;
    private int tempsTotal;
    private int numSimu;
    private MTRandom mtRandom;
    private SauveurEtatGrille sauveurEtatGrille;
    
    public Simulation(int numSimu, int tempsTotal, MTRandom mtRandom){
        this.mtRandom = mtRandom;
        this.tempsTotal = tempsTotal;
        this.numSimu = numSimu;
        sauveurEtatGrille = new SauveurEtatGrilleCSV(numSimu);
        initialiserGrille();
    }

    private void initialiserGrille(){
        grille = new Grille(300);
        ajouterAgentALaGrille(Statut.S, 19980);        
        ajouterAgentALaGrille(Statut.I, 20);
        tempsEcoule = 0;
    }

    /**
     * Permet d'ajouter un certain nombre d'agents à la grille
     * @param statut statuts des agents à ajouter
     * @param nbAgents nombre d'agents à ajouter
     */
    private void ajouterAgentALaGrille(Statut statut, int nbAgents){
        int x, y;
        for(int i = 0; i < nbAgents; i++){
            x = mtRandom.nextInt(0, 299);
            y = mtRandom.nextInt(0, 299);
            grille.getCellules()[x][y].ajouterAgent(new Agent(statut, mtRandom));
        }
    }

    private void evolutionEtatAgents(){
        int nbStatut[] = new int[4];
        for(int i = 0; i < grille.getDimension(); i++){
            for(int j = 0; j < grille.getDimension(); j++){
                for (Agent agentv : grille.getCellules()[i][j].getAgents()){
                    if(agentv.getStatut() == Statut.S){
                        agentv.verifierVoisinage(grille, j, i, mtRandom);
                        nbStatut[3]++;
                    }else{
                        agentv.verifierStatut(nbStatut);
                    }
                    agentv.incrementStatutTempsEcoule();
                }
            }
        }
        sauveurEtatGrille.sauverEtat(nbStatut);
    }

    private void deplacerAgents(){
        Agent agent;
        for(int i = 0; i < grille.getDimension(); i++){
            for(int j = 0; j < grille.getDimension(); j++){
                for (Iterator<Agent> iterator = grille.getCellules()[i][j].getAgents().iterator(); 
                iterator.hasNext();){
                    agent = iterator.next();
                    agent.deplacer(i, j, mtRandom, grille, iterator);
                }
            }
        }
    }

    public void lancerSimulation(){
        System.out.println("Début de la Simulation numéro : " + numSimu);
        for(tempsEcoule = 0; tempsEcoule < tempsTotal; tempsEcoule++){
            deplacerAgents();
            evolutionEtatAgents();
        }
        sauveurEtatGrille.fermerWriter();
        System.out.println("Fin de la Simulation numéro : " + numSimu);
    }
    
}
