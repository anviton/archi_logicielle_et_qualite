import java.util.LinkedList;
import java.util.List;

public class Cellule {
    private List<Agent> agents;

    public Cellule(){
        this.agents = new LinkedList<>();
    }

    public void ajouterAgent(Agent agent){
        agents.add(agent);
    }

    public void supprimerAgent(Agent agent){
        agents.remove(agent);
    }

    public List<Agent> getAgents(){
        return agents;
    }
}
